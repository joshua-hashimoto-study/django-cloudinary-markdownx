from django import forms
from markdownx.widgets import MarkdownxWidget

from .models import Article


class ArticleCreateForm(forms.ModelForm):

    class Meta:
        model = Article
        fields = (
            'cover',
            'title',
            'content',
        )
        widgets = {
            'content': MarkdownxWidget(attrs={'class': 'uk-textarea uk-margin'}),
        }
