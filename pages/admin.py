from django.contrib import admin
from markdownx.admin import MarkdownxModelAdmin

from .models import Article


class PagesAdmin(MarkdownxModelAdmin):
    pass


admin.site.register(Article, PagesAdmin)
