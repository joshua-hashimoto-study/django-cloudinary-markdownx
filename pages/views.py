import os
import uuid

from django.conf import settings
from django.http import HttpResponse, JsonResponse
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, DetailView

import cloudinary

from markdownx.views import ImageUploadView

from .forms import ArticleCreateForm
from .models import Article


class PageListView(ListView):
    model = Article
    template_name = 'pages/page_list.html'


class PageDetailView(DetailView):
    model = Article
    template_name = 'pages/page_detail.html'


class PageCreateView(CreateView):
    model = Article
    template_name = 'pages/page_new.html'
    form_class = ArticleCreateForm
    success_url = reverse_lazy('pages:page_home')


class CustomImageUploadView(ImageUploadView):

    def form_valid(self, form):
        print(self.request.POST)
        response = super(ImageUploadView, self).form_valid(form)

        if self.request.is_ajax():
            img_name = f'{uuid.uuid4().hex[:10]}-{form.cleaned_data.name.replace(" ", "-")}'
            form.cleaned_data.name = img_name
            img_folder = os.path.join(
                settings.MEDIA_URL, f'article/markdown/')
            # save image to cloudinary
            cloudinary_img = cloudinary.uploader.upload(
                form.cleaned_data, folder=img_folder, overwrite=True)
            # get the saved image url from cloudinary response
            cloudinary_img_path = cloudinary_img['secure_url']
            image_code = '![]({})'.format(cloudinary_img_path)
            return JsonResponse({'image_code': image_code})

        return response
