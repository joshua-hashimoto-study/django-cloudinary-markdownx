from django.urls import path

from .views import PageListView, PageDetailView, PageCreateView

app_name = 'pages'

urlpatterns = [
    path('', PageListView.as_view(), name='page_home'),
    path('new/', PageCreateView.as_view(), name='page_new'),
    path('<int:pk>/', PageDetailView.as_view(), name='page_detail'),
]
