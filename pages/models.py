from django.db import models
from django.shortcuts import reverse
from django.utils.safestring import mark_safe
from markdownx.models import MarkdownxField
from markdownx.utils import markdownify
from cloudinary_storage.storage import MediaCloudinaryStorage
# from cloudinary_storage.storage import VideoMediaCloudinaryStorage, MediaCloudinaryStorage
# from cloudinary_storage.validators import validate_video


def upload_image_to(instance, filename):
    """
    custom path for saving images

    Returns:
        str: image path
    """
    asset_path = f'article/{str(instance.title)}/images/{filename}'
    return asset_path


def upload_video_to(instance, filename):
    """
    custom path for saving videos

    Returns:
        str: video path
    """
    asset_path = f'article/{str(instance.title)}/video/{filename}'
    return asset_path


class Article(models.Model):
    # video = models.FileField(upload_to=upload_video_to, blank=True, null=True,
    #                          storage=VideoMediaCloudinaryStorage(), validators=[validate_video])
    cover = models.ImageField(
        upload_to=upload_image_to, blank=True, null=True, storage=MediaCloudinaryStorage())
    title = models.CharField(max_length=255)
    content = MarkdownxField()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("pages:page_detail", kwargs={"pk": self.pk})

    def get_content(self):
        content = self.content
        markdown_content = markdownify(content)
        return mark_safe(markdown_content)
