MARKDOWNX_MARKDOWN_EXTENSIONS = [
    'markdown.extensions.extra',
    'markdown.extensions.toc',  # TOC
    'markdown.extensions.tables',  # テーブル
    'markdown.extensions.nl2br',  # 改行
]
MARKDOWNX_IMAGE_MAX_SIZE = {
    'size': (1000, 1000),
    'quality': 90,
    'upscale': True,
}
MARKDOWNX_UPLOAD_CONTENT_TYPES = [
    'image/png', 'image/jpg',
    'image/jpeg', 'image/pjpeg', 'image/gif', 'image/svg+xml',
]
FORM_RENDERER = 'django.forms.renderers.TemplatesSetting'  # markdownxを横プレビューにするため
# from datetime import datetime
# MARKDOWNX_MEDIA_PATH = datetime.now().strftime('markdownx/%Y/%m/%d')
