from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from pages.views import CustomImageUploadView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('markdownx/upload/', CustomImageUploadView.as_view(),
         name='markdownx_upload'),
    path('markdownx/', include('markdownx.urls')),
    path('', include('pages.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
